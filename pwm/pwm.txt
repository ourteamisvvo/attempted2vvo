Configuration	pwm
STM32CubeMX 	5.4.0
Date	11/16/2019
MCU	STM32F303K8Tx



PERIPHERALS	MODES	FUNCTIONS	PINS
SYS	SysTick	SYS_VS_Systick	VP_SYS_VS_Systick
TIM3	Internal Clock	TIM3_VS_ClockSourceINT	VP_TIM3_VS_ClockSourceINT
TIM3	PWM Generation CH1	TIM3_CH1	PA6



Pin Nb	PINs	FUNCTIONs	LABELs
12	PA6	TIM3_CH1	
13	PA7	GPIO_Input	
PERIPHERALS	MODES	FUNCTIONS	PINS
SYS	SysTick	SYS_VS_Systick	VP_SYS_VS_Systick
TIM3	Internal Clock	TIM3_VS_ClockSourceINT	VP_TIM3_VS_ClockSourceINT
TIM3	PWM Generation CH1	TIM3_CH1	PA6



Pin Nb	PINs	FUNCTIONs	LABELs
12	PA6	TIM3_CH1	
13	PA7	GPIO_Input	



SOFTWARE PROJECT

Project Settings : 
Project Name : pwm
Project Folder : C:\Users\dalic\STM32CubeIDE\workspace_1.1.0\pwm
Toolchain / IDE : STM32CubeIDE
Firmware Package Name and Version : STM32Cube FW_F3 V1.11.0


Code Generation Settings : 
STM32Cube MCU packages and embedded software packs : Copy only the necessary library files
Generate peripheral initialization as a pair of '.c/.h' files per peripheral : No
Backup previously generated files when re-generating : No
Delete previously generated files when not re-generated : Yes
Set all free pins as analog (to optimize the power consumption) : No


Toolchains Settings : 
Compiler Optimizations : 





